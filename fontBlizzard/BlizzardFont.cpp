#include "BlizzardFont.h"
#include <iostream>

BlizzardFont::BlizzardFont(void)
{
}


BlizzardFont::~BlizzardFont(void)
{
}



BlizzardFont::BlizzardFont(std::string fontName)
{
	try{
		font.readFromFile(fontName);
//	header=new FontHeader;
		memcpy(&header,font.getBody(),sizeof(header));
		printHeader();
		letters=new FontLetter[header.HighIndex+1];
		int ofs=sizeof(header);
		for(int i=header.LowIndex;i<=header.HighIndex;i++)
		{
			printf("letter: %d '%hhc'\n", i, i);
			letters[i].init(ofs,font.getBody());
			if(!letters[i].isEmpty())
			{
				letters[i].printHeader();
				letters[i].unpackBody();
//				letters[i].printBody(1);
				letters[i].printBodyUnpacked(1);
				if(i>=128)
				{
/*					 // font6
					 letters[i].addShadow(1,0);
					 letters[i].addShadow(0,1);
					 letters[i].addShadow(1,1);
					 // font10x
					 letters[i].addShadow(0,2);
					 // font32
					 letters[i].addShadow(2,0);
					 letters[i].addShadow(2,1);
					 letters[i].addShadow(1,2);
					 letters[i].addShadow(0,3);
					 */
				}
				char letterFile[64];
				std::string letterName;
				if(fontName.substr(fontName.size()-4)==".fnt")
					letterName=fontName.substr(0,fontName.size()-4);
				else letterName=fontName;
//				sprintf_s(letterFile,"%s_%03d.bmp",letterName.c_str(),i);
				sprintf_s(letterFile,"%s_%2.2X.bmp",letterName.c_str(),i);
				letters[i].saveBmpUnpacked(letterFile);
			}
			ofs+=4;
		}
	}
   catch( char * str ) {
      std::cout << "Exception raised: " << str << '\n';
   }
}

int main(int argc, char **argv)
{
	if(argc==1)
	{
		BlizzardFont font("font10x.fnt");
	}
	else
	{
		BlizzardFont font(argv[1]);
	}
	return 0;
}


void BlizzardFont::printHeader(void)
{
	std::cout << "HighIndex: " << (int) header.HighIndex << " LowIndex: " << (int) header.LowIndex << " MaxHeight: " << (int) header.MaxHeight << 
		" MaxWidth: " << (int) header.MaxWidth << " Name: " << std::string((const char*)&header.Name,4) << std::endl;
}
