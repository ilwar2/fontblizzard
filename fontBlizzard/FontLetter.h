#pragma once

#include<windows.h>
#include<string>

#include "BString.h"
#include "bmp.h"

class FontLetter
{
private:
	typedef struct  _FontLetterRaw {
		BYTE	Width;		//	Width of the letter
		BYTE	Height;		//	Height of the letter
		BYTE	XOffset;	//	X Offset for the topleft corner of the letter.
		BYTE	YOffset;	//	Y Offset for the topleft corner of the letter.
	} FontLetterRaw;

	FontLetterRaw header;
	BString letter;
	BString letterUnpacked;
	bool _isEmpty;
	bool _isUnpacked;

	Bmp bmp;

public:
	FontLetter(void);
	~FontLetter(void);
	void init(int offset, char *base);
	void printHeader(void);
	void unpackBody();
	void printBody(int showLevel);
	void printBodyUnpacked(int showLevel);
	void saveBmpUnpacked(std::string fileName);
	bool isEmpty()
	{
		return _isEmpty;
	}
	bool isUnpacked()
	{
		return _isUnpacked;
	}
	void addShadow(int x, int y);
};

