#pragma once
#include <windows.h>

#define BG_COLOR 0x18
#define MAX_CHAR_SIZE 4108

// class based on functions from War2FontTool.cpp by Lambchops: http://forum.war2.ru/index.php/topic,4701.0.html
class Bmp
{
	BYTE* bmptr;
	BYTE* pixmap;
	int linew,bmw,bmh;
	int loadlen;

	BYTE *clrndx;

	int bmp8len;
	DWORD *bmp8;
public:

	Bmp(void);
	~Bmp(void);
	// initialize the bitmap buffer for the supplied size
	void init_bmp(int w, int h);
	void set_pixel(int x,int y,int c);
	void set_pixel_clrndx(int x,int y,int c);
	int get_pixel(int x,int y);
	// change palette indexes to font indexes
	//  change unknown values to transparent
	void normalize_bmp(void);
	int bmp_yos();
	int bmp_xos();
	BOOL save_bmp(char* path);
	BOOL load_bmp(char* path);
	BITMAPINFOHEADER* bmp_info();
};

