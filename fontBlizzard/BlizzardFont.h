#pragma once
#include<windows.h>
#include<string>

#include "BString.h"
#include "FontLetter.h"

class BlizzardFont
{
private:
	typedef struct	_FontHeader {
		DWORD	Name;		//	Always is "FONT"
		BYTE	LowIndex;	//	Index of first letter in file
		BYTE	HighIndex;	//	Index of the last letter in file
		BYTE	MaxWidth;	//	Maximum width
		BYTE	MaxHeight;	//	Maximum height
//		DWORD	Unk1;		//	Unknown / Unused
	} FontHeader;

	FontHeader header;
	BString font;
	FontLetter *letters;

public:
//	std::vector <unsigned char> fontBody;
	BlizzardFont(void);
	~BlizzardFont(void);
	BlizzardFont(std::string fontName);
//	std::vector<BYTE> readFile(std::string filename);
	void printHeader(void);
};
