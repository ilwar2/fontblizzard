#pragma once
#include<string>
#include<windows.h>

//using namespace std;

class BString
{
public:
	BString(void);
	~BString(void);
	int readFromFile(std::string fileName);
	char *getBody() {return body;}
private:
	long size;
	char *body;
public:
	int init(int size);
	int init(char * str, int size);
	void setByte(char c, int ofs);
};

