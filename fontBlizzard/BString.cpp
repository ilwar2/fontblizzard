#include "BString.h"
//#include <stdio.h>
#include <iostream>
#include <fstream>

BString::BString(void)
	: body(NULL)
{
}

BString::~BString(void)
{
	delete []body;
}


int BString::readFromFile(std::string fileName)
{
//	http://www.cplusplus.com/reference/istream/istream/read/

	std::ifstream is (fileName, std::ifstream::binary);
	if (is) {
		// get length of file:
		is.seekg (0, is.end);
		size = is.tellg();
		is.seekg (0, is.beg);

		body = new char [size];

		std::cout << "Reading " << size << " characters... ";
		// read data as a block:
		is.read (body,size);

		if (is)
			std::cout << "all characters read successfully.";
		else
			std::cout << "error: only " << is.gcount() << " could be read";
		is.close();

		// ...buffer contains the entire file...

//		delete[] buffer;
	}
	else throw("error opening file");
  return 0;

}


int BString::init(int len)
{
	size=len;
	body = new char [size];
	memset(body, 0, size);
	return 0;
}
int BString::init(char * str, int len)
{
	size=len;
	body = new char [size];
	memcpy(body, str, size);
	return 0;
}
void BString::setByte(char c, int ofs)
{
	body[ofs]=c;
}