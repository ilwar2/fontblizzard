#include "FontLetter.h"
#include <iostream>
#include<stdio.h>


FontLetter::FontLetter(void)
{
	_isEmpty=true;
	_isUnpacked=false;
}


FontLetter::~FontLetter(void)
{
}


void FontLetter::init(int offset, char *base)
{
	DWORD ofs;
	memcpy(&ofs,base+offset,sizeof(DWORD));
	printf("ofs: %x\n",ofs);
	if(ofs!=0)
	{
		_isEmpty=false;
		memcpy(&header,base+ofs,sizeof(header));
		letter.init(base+ofs+sizeof(header),header.Width*header.Height);
	}
	bmp.init_bmp(header.Width+header.XOffset,header.Height+header.YOffset);
}


void FontLetter::printHeader(void)
{
	if(isEmpty())
	{
		printf("skip\n");
	}
	else
	{
		printf("W: %hhu, H: %hhu, X: %hhu, Y: %hhu\n",header.Width,header.Height,header.XOffset,header.YOffset);
		if(header.XOffset!=0)printf("XOffset!=0\n");
	}
}


void FontLetter::unpackBody()
{
	if(!isEmpty())
	{
		int toSkip=0;
		letterUnpacked.init(header.Width*header.Height);

		unsigned char l,point;
		int bodyOfs=0;
		int unpackedOfs=0;
		for(int i=0;i<header.Height*header.Width;i++)
		{
			if(toSkip==0)
			{
				l=letter.getBody()[bodyOfs];
				point=l&7;
				toSkip=l>>3;
				bodyOfs++;
			}
			else
			{
				toSkip--;
			}
			if(toSkip!=0)
			{
				letterUnpacked.setByte(8,unpackedOfs++);
			}
			else
			{
				letterUnpacked.setByte(point,unpackedOfs++);
			}
		}
		_isUnpacked=true;
	}
}
void FontLetter::printBody(int showLevel)
{
	int toSkip=0;
	if(isEmpty())
	{
		printf("skip\n");
	}
	else
	{
		unsigned char l,point;
		int bodyOfs=0;
		for(int i=0;i<header.Height*header.Width;i++)
		{
			if(toSkip==0)
			{
				l=letter.getBody()[bodyOfs];
				point=l&7;
				toSkip=l>>3;
				bodyOfs++;
			}
			else
			{
				toSkip--;
			}
			if(toSkip!=0)
			{
				if(showLevel<1)printf("  ");
				else printf("--");
			}
			else
			{
				if(point==5)
				{
					if(showLevel<1)printf("  ");
					else printf("%2hhx",point);
				}
				else
				{
					printf("%2hhx",point);
				}
			}
			if(i%header.Width==header.Width-1)printf("\n");
		}
		printf("\n");
	}
}
void FontLetter::printBodyUnpacked(int showLevel)
{
	if(isEmpty()||!isUnpacked())
	{
		printf("skip\n");
	}
	else
	{
		unsigned char point;
		for(int i=0;i<header.Height*header.Width;i++)
		{
			point=letterUnpacked.getBody()[i];
			if(point==8)
			{
				if(showLevel<1)printf("  ");
				else printf("--");
			}
			else
			{
				if(point==5)
				{
					if(showLevel<1)printf("  ");
					else printf("%2hhx",point);
				}
				else
				{
					printf("%2hhx",point);
				}
			}
			if(i%header.Width==header.Width-1)printf("\n");
		}
		printf("\n");
	}
}
void FontLetter::saveBmpUnpacked(std::string fileName)
{
	if(!isEmpty()&&isUnpacked())
	{
		unsigned char point;
//		int x=header.XOffset,y=header.YOffset;
		int x=0,y=0;
		int ofs=0;
		for(int i=0;i<header.Height*header.Width;i++)
		{
			point=letterUnpacked.getBody()[i];
			if(point!=8)bmp.set_pixel_clrndx(x+header.XOffset,y+header.YOffset,point);
			if(i%header.Width==header.Width-1)
			{
				x=0;
				y++;
			}
			else
			{
				x++;
			}
		}
		bmp.save_bmp((char *)fileName.c_str());
	}
}

void FontLetter::addShadow(int x, int y)
{
	if(!isEmpty()&&isUnpacked())
	{
		for(int i=0;i<header.Height*header.Width;i++)
		{
			switch(letterUnpacked.getBody()[i])
			{
			case 1:
			case 2:
			case 3:
			case 4:
				if(i%header.Width+x<header.Width && i/header.Width+y<header.Height)
				{
					int j=i+header.Width*y+x;
					if(j>=header.Height*header.Width)
					{
						printf("Error! i=%d, j=%d, w=%d,h=%d",i,j,header.Width,header.Height);
						exit(1);
					}
					switch(letterUnpacked.getBody()[j])
					{
					case 8:
					case 0:
						letterUnpacked.setByte(5,j);
					}
				}
			}
		}
	}
}
